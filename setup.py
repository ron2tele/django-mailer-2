from distutils.core import setup
from django_mailer import get_version


setup(
    name='django-mailer-2',
    version=get_version(),
    description=("A reusable Django app for queueing the sending of email "
                 "(forked from Chris Beaven's django-mailer-2)"),
    long_description=open('docs/usage.rst').read(),
    author='Mbadiwe Nnaemeka Ronald',
    author_email='ron2tele@gmail.com',
    url='http://github.com/ron4fun/django-mailer-2',
    packages=[
        'django_mailer',
        'django_mailer.management',
        'django_mailer.management.commands'
    ],
    classifiers=[
        "Development Status :: 6 - Production/Stable",
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Framework :: Django",
    ],
    install_requires=[
        'Django >= 1.2',
        'lockfile >= 0.8',
        'six',
        ],
)
