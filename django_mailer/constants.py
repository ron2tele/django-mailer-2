PRIORITY_EMAIL_NOW = 0
PRIORITY_HIGH = 1
PRIORITY_MEDIUM = 3
PRIORITY_LOW = 5
PRIORITY_DEFERRED = 7

RESULT_SENT = 0
RESULT_FAILED = 1

PRIORITIES = {
    'now': PRIORITY_EMAIL_NOW,
    'high': PRIORITY_HIGH,
    'medium': PRIORITY_MEDIUM,
    'low': PRIORITY_LOW,
    "deferred": PRIORITY_DEFERRED,
}

try:
    from django.core.mail import get_connection
    EMAIL_BACKEND_SUPPORT = True
except ImportError:
    # Django version < 1.2
    EMAIL_BACKEND_SUPPORT = False
