from __future__ import unicode_literals
import logging
from django.core.management.base import BaseCommand
from django_mailer import models
from django_mailer.constants import RESULT_SENT, RESULT_FAILED
from django_mailer.management.commands import create_file_handler, create_anon_handler
from django.db import connection


RESULT_CODES = {
    'success': RESULT_SENT, 
    'failure': RESULT_FAILED, 
    'all': [RESULT_SENT, RESULT_FAILED]
}


class Command(BaseCommand):
    help = "Delete django_mailer log"

    def add_arguments(self, parser):
        parser.add_argument('-d', '--days', type=int, default=30)
        parser.add_argument('-r', '--result', choices=RESULT_CODES.keys(),
                            help='Delete logs of messages with the given result code(s) '
                                 '(default: success)')
        parser.add_argument('-f', '--file', type=str, default="",
            help='Set full path to the logging output file.')

    def handle(self, *args, **options):
        days = options['days']
        result = RESULT_CODES.get(options['result'])
        file = options['file']
        verbosity = options['verbosity']
    
        # Send logged messages to the console.
        logger = logging.getLogger('django_mailer')
        handler = None
        if file:
            handler = create_file_handler(verbosity, file)
        else:
            handler = create_anon_handler(verbosity)

        logger.addHandler(handler)

        count = models.MessageLog.objects.purge_old_entries(days, result)

        logger.info("%s log entries deleted." % count)

        if handler:
            logger.removeHandler(handler)

        # Stop superfluous "unexpected EOF on client connection" errors in
        # Postgres log files caused by the database connection not being
        # explicitly closed.
        connection.close()
