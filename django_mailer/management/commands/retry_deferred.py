import logging
from django.core.management.base import BaseCommand
from django_mailer import models
from django_mailer.management.commands import create_file_handler, create_anon_handler
from django.db import connection

class Command(BaseCommand):
    help = 'Place deferred messages back in the queue.'

    def add_arguments(self, parser):
        parser.add_argument('-m', '--max-retries', type=int,
            help="Don't reset deferred messages with more than this many "
                "retries.")
        parser.add_argument('-f', '--file', type=str, default="",
            help='Set full path to the logging output file.')

    def handle(self, *args, **options):
        max_retries = options['max_retries']
        file = options['file']
        verbosity = options['verbosity']
    
        # Send logged messages to the console.
        logger = logging.getLogger('django_mailer')
        handler = None
        if file:
            handler = create_file_handler(verbosity, file)
        else:
            handler = create_anon_handler(verbosity)

        logger.addHandler(handler)

        count = models.Message.objects.retry_deferred(
                                                    max_retries=max_retries)
        logger.info("%s deferred message%s placed back in the queue.\n" %
                       (count, count != 1 and 's' or ''))                   

        if handler:
            logger.removeHandler(handler)

        # Stop superfluous "unexpected EOF on client connection" errors in
        # Postgres log files caused by the database connection not being
        # explicitly closed.
        connection.close()

