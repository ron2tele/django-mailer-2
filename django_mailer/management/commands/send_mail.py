from django.core.management.base import BaseCommand
from django.db import connection
from django_mailer import models, settings
from django_mailer.engine import send_all
from django_mailer.management.commands import create_file_handler, create_anon_handler
import logging

class Command(BaseCommand):
    help = 'Iterate the mail queue, attempting to send all mail.'

    def add_arguments(self, parser):
        parser.add_argument('-c', '--count', action='store_true', default=False,
            help='Return the number of messages in the queue (without '
                'actually sending any).')
        parser.add_argument('-f', '--file', type=str, default="",
            help='Set full path to the logging output file.')

    def handle(self, *args, **options):
        count = options['count']
        file = options['file']
        verbosity = options['verbosity']

        logger = logging.getLogger('django_mailer')

        # Send logged messages to the console.        
        handler = None
        if file:
            handler = create_file_handler(verbosity, file)
        else:
            handler = create_anon_handler(verbosity)

        logger.addHandler(handler)

        # If this is just a count request the just calculate, report and exit.
        if count:
            queued = models.Message.objects.non_deferred().count()
            deferred = models.Message.objects.deferred().count()
            logger.info('%s queued message%s (and %s deferred message%s).'
                            % (queued, queued != 1 and 's' or '',
                                     deferred, deferred != 1 and 's' or ''))
        else:        
            logger.info("-" * 72)
            # if PAUSE_SEND is turned on don't do anything.
            if not settings.PAUSE_SEND:
                send_all()
            else:
                logger = logging.getLogger('django_mailer.commands.send_mail')
                logger.info("sending is paused, quitting.")
            
        if handler:
            logger.removeHandler(handler)

        # Stop superfluous "unexpected EOF on client connection" errors in
        # Postgres log files caused by the database connection not being
        # explicitly closed.
        connection.close()
