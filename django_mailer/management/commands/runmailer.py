from django.utils import timezone

from django.core.management import BaseCommand

from django.db import connection
from django_mailer.engine import send_loop
from django_mailer.management.commands import create_file_handler, create_anon_handler
import sys
import logging


class Command(BaseCommand):
    """Start the django-mailer send loop"""

    def add_arguments(self, parser):
        parser.add_argument('-f', '--file', type=str,
            help='Set full path to the logging output file.')

    def handle(self, *args, **options):
        file = options['file']
        verbosity = options['verbosity']

        # Send logged messages to the console.
        logger = logging.getLogger('django_mailer')
        handler = None
        if file:
            handler = create_file_handler(verbosity, file)
        else:
            handler = create_anon_handler(verbosity)

        logger.addHandler(handler)

        self.stdout.write(timezone.now().strftime('%B %d, %Y - %X'))
        self.stdout.write('Starting django-mailer send loop.')
        quit_command = 'CTRL-BREAK' if sys.platform == 'win32' else 'CONTROL-C'
        self.stdout.write('Quit the loop with %s.' % quit_command)
        send_loop()

        if handler:
            logger.removeHandler(handler)

        # Stop superfluous "unexpected EOF on client connection" errors in
        # Postgres log files caused by the database connection not being
        # explicitly closed.
        connection.close()