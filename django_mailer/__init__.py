import logging

VERSION = (2, 5, 0)

logger = logging.getLogger('django_mailer')
logger.setLevel(logging.DEBUG)

import warnings

import six

def get_version():
    bits = [str(bit) for bit in VERSION]
    version = bits[0]
    for bit in bits[1:]:
        version += (bit.isdigit() and '.' or '') + bit
    return version


def get_priority(priority):
    from django_mailer.models import PRIORITY_MAPPING, PRIORITY_MEDIUM
    if priority is None:
        priority = PRIORITY_MEDIUM

    if priority in PRIORITY_MAPPING:
        warnings.warn("Please pass one of the PRIORITY_* constants to 'send_mail' "
                      "and 'send_html_mail', not '{0}'.".format(priority),
                      DeprecationWarning)
        priority = PRIORITY_MAPPING[priority]
    if priority not in PRIORITY_MAPPING.values():
        raise ValueError("Invalid priority {0}".format(repr(priority)))
    return priority


def build_EmailMessage(subject="", body="", from_email=None, to=None, bcc=None,
                 attachments=None, headers={}, reply_to=None):
    """
    Creates an EmailMessage for the email parameters supplied.
    """ 

    from django.core.mail import EmailMessage
    if six.PY2:
        # Only runs Django 1.11
        from django.utils.encoding import force_unicode as force_str
    else:
        from django.utils.encoding import force_str

    # need to do this in case subject used lazy version of ugettext
    subject = force_str(subject)
    body = force_str(body)
    
    return EmailMessage(
            subject=subject,
            body=body,
            from_email=from_email,
            to=to,
            bcc=bcc,
            attachments=attachments,
            headers=headers,
            reply_to=reply_to,
        )


def send_message_now(email_message):
    from django_mailer.constants import EMAIL_BACKEND_SUPPORT, RESULT_SENT

    if EMAIL_BACKEND_SUPPORT:
        from django_mailer.engine import send_message
        result = send_message(email_message)
        return (result == RESULT_SENT)
    else:
        return email_message.send()


def send_mail(subject, message, from_email, recipient_list, priority=None,
              fail_silently=False, auth_user=None, auth_password=None, 
              attachments=None, headers={}, bcc=None, reply_to=None):
    from django_mailer.models import make_message
    from django_mailer import constants

    priority = get_priority(priority)

    email_message = build_EmailMessage(
                subject=subject,
                body=message,
                from_email=from_email,
                to=recipient_list,
                bcc=bcc,
                headers=headers,
                reply_to=reply_to,
                attachments=attachments
            )
    
    if priority == constants.PRIORITY_EMAIL_NOW:
        return send_message_now(email_message)

    msg = make_message(email_message, priority=priority)
    msg.save()

    if priority == constants.PRIORITY_DEFERRED:
        msg.defer()

    return 1


def send_html_mail(subject, message, message_html, from_email, recipient_list,
                   priority=None, fail_silently=False, auth_user=None,
                   auth_password=None, headers={}, bcc=None, 
                   attachments=None, reply_to=None):
    """
    Function to queue HTML e-mails
    """
    
    from django.core.mail import EmailMultiAlternatives
    from django_mailer.models import make_message
    from django_mailer import constants

    priority = get_priority(priority)

    email_message = build_EmailMessage(
                subject=subject,
                body=message,
                from_email=from_email,
                to=recipient_list,
                bcc=bcc,
                headers=headers,
                reply_to=reply_to,
                attachments=attachments
            )

    email_html_message = EmailMultiAlternatives(
        subject=subject,
        body=message,
        from_email=from_email,
        to=recipient_list,
        bcc=bcc,
        headers=headers,
        reply_to=reply_to,
        attachments=attachments
    )
    email_html_message.attach_alternative(message_html, "text/html")

    if priority == constants.PRIORITY_EMAIL_NOW:
        return send_message_now(email_html_message)

    msg = make_message(email_message, priority=priority)

    msg.email = email_html_message
    msg.save()

    if priority == constants.PRIORITY_DEFERRED:
        msg.defer()

    return 1


def send_mass_mail(datatuple, fail_silently=False, auth_user=None,
                   auth_password=None, connection=None):
    num_sent = 0
    for subject, message, sender, recipient in datatuple:
        num_sent += send_mail(subject, message, sender, recipient)
    return num_sent


def mail_admins(subject, message, fail_silently=False, priority=None):
    """
    Add one or more new messages to the mail queue addressed to the site
    administrators (defined in ``settings.ADMINS``).

    This is a replacement for Django's ``mail_admins`` core email method.

    The ``fail_silently`` argument is only provided to match the signature of
    the emulated function. This argument is not used.

    """
    from django.conf import settings as django_settings
    from django.utils.encoding import force_unicode
    from django_mailer import settings

    if priority is None:
        settings.MAIL_ADMINS_PRIORITY

    subject = django_settings.EMAIL_SUBJECT_PREFIX + force_unicode(subject)
    from_email = django_settings.SERVER_EMAIL
    recipient_list = [recipient[1] for recipient in django_settings.ADMINS]
    send_mail(subject, message, from_email, recipient_list, priority=priority)


def mail_managers(subject, message, fail_silently=False, priority=None):
    """
    Add one or more new messages to the mail queue addressed to the site
    managers (defined in ``settings.MANAGERS``).

    This is a replacement for Django's ``mail_managers`` core email method.

    The ``fail_silently`` argument is only provided to match the signature of
    the emulated function. This argument is not used.

    """
    from django.conf import settings as django_settings
    from django.utils.encoding import force_unicode
    from django_mailer import settings

    if priority is None:
        priority = settings.MAIL_MANAGERS_PRIORITY

    subject = django_settings.EMAIL_SUBJECT_PREFIX + force_unicode(subject)
    from_email = django_settings.SERVER_EMAIL
    recipient_list = [recipient[1] for recipient in django_settings.MANAGERS]
    send_mail(subject, message, from_email, recipient_list, priority=priority)
