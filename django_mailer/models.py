from __future__ import unicode_literals

from django.core.mail import EmailMessage

import base64
import logging
import pickle

import six

if six.PY2:
    from django.utils.translation import ugettext_lazy as _
else:
    from django.utils.translation import gettext_lazy as _

from django.db import models
from django.utils.timezone import now as datetime_now
import datetime

try:
    from django.utils.encoding import python_2_unicode_compatible
except ImportError:
    def python_2_unicode_compatible(c):
        return c

from django_mailer.constants import (PRIORITY_DEFERRED, PRIORITY_HIGH, PRIORITY_LOW, 
                                PRIORITY_EMAIL_NOW, PRIORITY_MEDIUM, RESULT_FAILED, RESULT_SENT)

logger = logging.getLogger(__name__)


PRIORITIES = (
    (PRIORITY_EMAIL_NOW, 'now'),
    (PRIORITY_HIGH, 'high'),
    (PRIORITY_MEDIUM, 'medium'),
    (PRIORITY_LOW, 'low'),
    (PRIORITY_DEFERRED, 'deferred'),
)

PRIORITY_MAPPING = dict((label, v) for (v, label) in PRIORITIES)

PRIORITY_MEDIUM = PRIORITY_MEDIUM
PRIORITY_DEFERRED = PRIORITY_DEFERRED

RESULT_CODES = (
    (RESULT_SENT, 'success'),
    (RESULT_FAILED, 'failure'),
)


base64_encode = base64.encodebytes if hasattr(base64, 'encodebytes') else base64.encodestring
base64_decode = base64.decodebytes if hasattr(base64, 'decodebytes') else base64.decodestring


def email_to_db(email):
    # pickle.dumps returns essentially binary data which we need to base64
    # encode to store in a unicode field. finally we encode back to make sure
    # we only try to insert unicode strings into the db, since we use a
    # TextField
    return base64_encode(pickle.dumps(email)).decode('ascii')

def db_to_email(data):
    if data == "":
        return None
    else:
        try:
            data = data.encode("ascii")
        except AttributeError:
            pass

        try:
            return pickle.loads(base64_decode(data))
        except (TypeError, pickle.UnpicklingError, base64.binascii.Error, AttributeError):
            try:
                # previous method was to just do pickle.dumps(val)
                return pickle.loads(data)
            except (TypeError, pickle.UnpicklingError, AttributeError):
                return None

def filter_recipient_list(lst):
    if lst is None:
        return None
        
    # get all blacklisted emails
    blacklist = Blacklist.objects.values_list('email', flat=True)
    
    good = []
    bad = []
    for e in lst:
        if e in blacklist:
            bad.append(e)    
        else:
            good.append(e)
            
    return good, bad

def get_message_id(msg):
    # From django.core.mail.message: Email header names are case-insensitive
    # (RFC 2045), so we have to accommodate that when doing comparisons.
    for key, value in msg.extra_headers.items():
        if key.lower() == 'message-id':
            return value

def make_message(email_message, priority=None):
    """
    Creates a simple message for the email parameters supplied.

    If needed, the 'email' attribute can be set to any instance of EmailMessage
    if e-mails with attachments etc. need to be supported.

    Call 'save()' on the result when it is ready to be sent, and not before.
    """        
   
    db_msg = Message(priority=priority)
    db_msg.email = email_message
    return db_msg




class MessageManager(models.Manager):
    
    def exclude_future(self):
        """
        Exclude future time-delayed messages.

        """
        return self.exclude(date_queued__gt=datetime_now)
        
    def high_priority(self):
        """
        the high priority messages in the queue
        """
        return self.filter(priority=PRIORITY_HIGH)

    def medium_priority(self):
        """
        the medium priority messages in the queue
        """
        return self.filter(priority=PRIORITY_MEDIUM)

    def low_priority(self):
        """
        the low priority messages in the queue
        """
        return self.filter(priority=PRIORITY_LOW)

    def non_deferred(self):
        """
        the messages in the queue not deferred
        """
        return self.exclude(priority=PRIORITY_DEFERRED)

    def deferred(self):
        """
        the deferred messages in the queue
        """
        return self.filter(priority=PRIORITY_DEFERRED)

    def retry_deferred(self, max_retries=None, new_priority=PRIORITY_MEDIUM):
        queryset = self.deferred()
        if max_retries:
            queryset = queryset.filter(retries__lte=max_retries)

        return queryset.update(priority=new_priority, retries=models.F('retries')+1)


class MessageLogManager(models.Manager):

    def log(self, message, result_code, log_message=""):
        """
        create a log entry for an attempt to send the given message and
        record the given result and (optionally) a log message
        """
        return self.create(
            message_data=message.message_data,
            message_id=get_message_id(message.email),
            when_added=message.created_at,
            priority=message.priority,
            result=result_code,
            log_message=log_message,
        )

    def purge_old_entries(self, days, result_codes=None):
        query = None
        limit = datetime_now() - datetime.timedelta(days=days)

        if result_codes is None:
            result_codes = RESULT_SENT
          
        if type(result_codes) is int:
            query = self.filter(when_attempted__lt=limit, result=result_codes)
        elif type(result_codes) is list:
            query = self.filter(when_attempted__lt=limit, result__in=result_codes)
        else:
            raise ValueError("Error parsing <result_codes>")                

        count = query.count()
        query.delete()
        return count

       
class BlacklistManager(models.Manager):  
    def has_address(self, address):
        """
        is the given address on the don't send list?
        """
        queryset = self.filter(email__iexact=address)
        return queryset.exists()



@python_2_unicode_compatible
class Message(models.Model):
    """
    The email stored for later sending.
    """

    # The actual data - a pickled EmailMessage
    message_data = models.TextField(_("Data"), help_text=_("The actual data - a pickled EmailMessage"))
    created_at = models.DateTimeField(_("created at"), default=datetime_now)
    priority = models.PositiveSmallIntegerField(_("priority"), choices=PRIORITIES, default=PRIORITY_MEDIUM)

    deferred = models.DateTimeField(_("deferred at"), null=True, blank=True, help_text=_("When mail was last sent but failed."))
    retries = models.PositiveIntegerField(_("retries"), default=0, help_text=_("Represents how many failed attempts when sending mail."))

    objects = MessageManager()

    class Meta:
        verbose_name = _("message")
        verbose_name_plural = _("messages")
        ordering = ('created_at',)

    def __str__(self):
        try:
            email = self.email
            return "On {0}, \"{1}\" to {2}".format(self.created_at,
                                                   email.subject,
                                                   ", ".join(email.to))
        except Exception:
            return "<Message repr unavailable>"

    def defer(self):
        self.deferred = datetime_now()
        self.priority = PRIORITY_DEFERRED
        self.save()

    def _get_email(self):
        return db_to_email(self.message_data)

    def _set_email(self, val):
        self.message_data = email_to_db(val)

    email = property(
        _get_email,
        _set_email,
        doc="""EmailMessage object. If this is mutated, you will need to
        set the attribute again to cause the underlying serialised data to be updated.""")

    @property
    def to_addresses(self):
        email = self.email
        if email is not None:
            return email.to
        else:
            return []

    @property
    def subject(self):
        email = self.email
        if email is not None:
            return email.subject
        else:
            return ""


@python_2_unicode_compatible
class MessageLog(models.Model):
    """
    A log entry which stores the result (and optionally a log message) for an
    attempt to send a Message.
    """

    # fields from Message
    message_data = models.TextField(_("Data"), help_text=_("The actual data - a pickled EmailMessage"))
    message_id = models.TextField(_("Message-ID"), editable=False, null=True, help_text=_("A unique identifier included in the header section of each attempted mail."))
    when_added = models.DateTimeField(_("queued at"), db_index=True, help_text=_("Represents when the mail was created."))
    priority = models.PositiveSmallIntegerField(_("priority"), choices=PRIORITIES, db_index=True)

    # additional logging fields
    when_attempted = models.DateTimeField(_("attempted at"), default=datetime_now, help_text=_("Represents when the mail was attempted."))
    result = models.PositiveSmallIntegerField(_("result"), choices=RESULT_CODES)
    log_message = models.TextField(_("log"), help_text=_("Delivery errors."))

    objects = MessageLogManager()    

    class Meta:
        verbose_name = _("message log")
        verbose_name_plural = _("message logs")
        ordering = ("when_attempted",)

    def __str__(self):
        try:
            email = self.email
            return "On {0}, \"{1}\" to {2}".format(self.when_attempted,
                                                   email.subject,
                                                   ", ".join(email.to))
        except Exception:
            return "<MessageLog repr unavailable>"

    @property
    def email(self):
        return db_to_email(self.message_data)

    @property
    def to_addresses(self):
        email = self.email
        if email is not None:
            return email.to
        else:
            return []

    @property
    def subject(self):
        email = self.email
        if email is not None:
            return email.subject
        else:
            return ""


class Blacklist(models.Model):
    """
    A blacklisted email address.
    
    Messages attempted to be sent to e-mail addresses which appear on this
    blacklist will be skipped entirely.
    
    """
    email = models.EmailField(max_length=200)
    date_added = models.DateTimeField(_("created at"), default=datetime_now)

    objects = BlacklistManager()

    class Meta:
        ordering = ('-date_added',)
        verbose_name = 'blacklisted e-mail address'
        verbose_name_plural = 'blacklisted e-mail addresses'

