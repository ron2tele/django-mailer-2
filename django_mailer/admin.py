from django.contrib import admin
from django_mailer import models


class MessageAdminMixin(object):

    def plain_text_body(self, message):
        email = message.email
        if hasattr(email, 'body'):
            return email.body
        else:
            return "<Can't decode>"

    def show_to(self, message):
        return ", ".join(message.to_addresses)
    show_to.short_description = "To"  # noqa: E305


class MessageAdmin(MessageAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("subject", "show_to", "created_at", "priority", "retries", "deferred", "plain_text_body")}),
    )

    def not_deferred(self, obj):
        return not obj.deferred
    not_deferred.boolean = True
    not_deferred.admin_order_field = 'deferred'
    
    list_display = ["show_to", "subject", "created_at", "retries", "priority", "not_deferred"]
    readonly_fields = ['plain_text_body', 'retries', 'deferred']
    exclude = ['message_data']
    date_hierarchy = "created_at"
    readonly_fields = ['show_to', 'subject']
    list_filter = ["created_at", "deferred"]
    ordering = ['priority']

class BlacklistAdmin(admin.ModelAdmin):
    list_display = ('email', 'date_added')
    search_fields = ['email']
    date_hierarchy = "date_added"
    list_filter = ["date_added"]

class MessageLogAdmin(MessageAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ("message_id", "subject", "show_to", "when_added", "priority", "when_attempted", "result", "log_message", "plain_text_body")}),
    )
    list_display = ["message_id", "when_added", "show_to", "subject", "priority", "when_attempted", "sent"]
    list_filter = ["result", "when_added"]
    date_hierarchy = "when_attempted"
    readonly_fields = ['plain_text_body', 'message_id', 'show_to', 'subject']
    exclude = ['message_data']
    search_fields = ['message_id']
    list_display_links = ['message_id']
    ordering = ["priority"]

    def sent(self, obj):
        return not obj.result
    sent.boolean = True

admin.site.register(models.Message, MessageAdmin)
admin.site.register(models.Blacklist, BlacklistAdmin)
admin.site.register(models.MessageLog, MessageLogAdmin)
