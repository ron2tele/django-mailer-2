from __future__ import unicode_literals

from django.core.mail.backends.base import BaseEmailBackend

from django_mailer import models, settings


class EmailBackend(BaseEmailBackend):
    '''
    A wrapper that manages a queued SMTP system.

    '''

    def send_messages(self, email_messages):
        """
            Allows for a custom batch size
        """
        messages = models.Message.objects.bulk_create([
            models.Message(email=email) for email in email_messages
        ], settings.MESSAGES_BATCH_SIZE)

        return len(messages)
